def givemereq():

  # Load from settings
  import os, sys
  import urllib

  sys.path.insert(0, "/srv/modules")
  from settings import * #this loads: Klogin, Kpassword, Kcontractid
  from __main__ import *

  # Static variables
  Kreturnmode = "urlencode"
  Ktxtype = "TRANSACTION"
  Kchannel = "testchannel"
  Ktag = "ideal"
  Kmerchanttxid = "pd1"
  Ksoldservice = "Ahoyworld.net Donation"
  Kmerchantredirecturl = "https://paydrive.ahoyworld.net/confirm/"
  Kmerchanterrorurl = "https://paydrive.ahoyworld.net/whoopsie/"
  Knotificationurl = "https://paydrive.ahoyworld.net/notification/"
  Kreq = ""

  # Build  variable string
  Kvars = [Kreturnmode, Ktxtype, Klogin, Kpassword, Kcontractid, Kchannel, Ktag, Kmerchanttxid, Ksoldservice, Kmerchantredirecturl, Kmerchanterrorurl, Knotificationurl, paymentmethod, amount, currency, countrycode, accountholdername, dynamicdescriptor ]

  for index, item in enumerate(Kvars):
    #item = urllib.quote_plus(item)
    if index == 0: item = "returnmode="+str(item)
    if index == 1: item = 'txtype='+str(item)
    if index == 2: item = 'login='+str(item)
    if index == 3: item = 'password='+str(item)
    if index == 4: item = 'contractid='+str(item)
    if index == 5: item = 'channel='+str(item)
    if index == 6: item = 'tag='+str(item)
    if index == 7: item = 'merchanttxid='+str(item)
    if index == 8: item = 'soldservice='+str(item)
    if index == 9: item = 'merchantredirecturl='+str(item)
    if index == 10: item = 'merchanterrorurl='+str(item)
    if index == 11: item = 'notificationurl='+str(item)
    if index == 12: item = 'paymentmethod='+str(item)
    if index == 13: item = "amount="+str(item)
    if index == 14: item = 'currency='+str(item)
    if index == 15: item = 'countrycode='+str(item)
    if index == 16: item = 'accountholdername='+str(item)
    if index == 17: item = 'specin.dynamicdescriptor='+str(item)
    Kvars[index] = item

  for index, item in enumerate(Kvars):
    Kreq = Kreq+'&'+item
  Kreq = Kreq[1:]
  return Kreq
