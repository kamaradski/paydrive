# -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
# File:                 pd_submit.py
# Version:              V0.1
# Contributors: None
#
# Submit script for PayDrive payment module
#
# -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-


# -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
# Loading things
# -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
import sys
import web
import urllib

# -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
# Web
# -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
sys.path.insert(0, "/srv/paydrive/mods")

urls = (
  '/', 'Index',
  '/result', 'result'
)

app = web.application(urls, globals())
render = web.template.render('templates/')

class Index(object):
    def GET(self):
        return render.index()

class result(object):
    def GET(self):
        return render.result()

    def POST(self):
        form = web.input()
	paymentmethod = form.paymentmethod
	#return paymentmethod  #debugtest to see if paymentmethod exists at this point
	import createrequest
	Kreq = createrequest.givemereq()
        return render.result(Kreq = Kreq)

if __name__ == "__main__":
    app.run()
